# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [0.0.1] - 2020-02-17

### Added

- Set up a complete pipeline with best pratices regarding:
  - Documentation
  - Contributions files
  - CI/CD
  - Auditing

[0.0.1]: https://gitlab.com/beescreens/beescreens-nms-server/-/releases#0.0.1
