importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

const ALLOWED_HOSTS = [
	// The domain to load markdown files
	location.host,
	// The domain to load docute
	'unpkg.com',
];

const matchCb = ({ url, event }) => event.request.method === 'GET' && ALLOWED_HOSTS.includes(url.host);

workbox.routing.registerRoute(
	matchCb,
	new workbox.strategies.NetworkFirst(),
);
